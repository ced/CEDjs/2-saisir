Traduction de l'application [CEDy](https://framagit.org/ced/cedy/2-saisir) (Calculette : Eco-déplacements, écrite en Python)
dans le langage javascript...

Voir la [demo](https://ced.frama.io/CEDjs/2-saisir/) mise
en œuvre via les gitlab pages et l'intégration continue...
